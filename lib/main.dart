import 'package:animation/screen/home_screen.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Animation Demo',
      theme: ThemeData(
        brightness: Brightness.dark,
        useMaterial3: true,
      ),
      debugShowCheckedModeBanner: false,
      debugShowMaterialGrid: false,
      themeMode: ThemeMode.dark,
      // home: const MyHomePage(),
      // home: const FlashMessageScreen(),
      // home: const ChainedAnimationPage(),
      // home: const ThreeDAnimation(),
      home: const DisplayAnimation(),
    );
  }
}
