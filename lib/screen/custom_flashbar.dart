import 'dart:math';

import 'package:flutter/material.dart';

class FlashMessageScreen extends StatelessWidget {
  const FlashMessageScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
          child: ElevatedButton(
        onPressed: () {
          ScaffoldMessenger.of(context).clearSnackBars();
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Stack(
              clipBehavior: Clip.none,
              children: [
                Container(
                  padding: const EdgeInsets.all(16),
                  height: 100,
                  decoration: BoxDecoration(
                      color: const Color(0xFFC72C41),
                      borderRadius: BorderRadius.circular(20)),
                  child: const Row(
                    children: [
                      SizedBox(width: 48),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Oh snap!",
                              style:
                                  TextStyle(fontSize: 18, color: Colors.white),
                            ),
                            // Spacer(),
                            Text(
                              "jhgvshdgfjkgbsdkuvgsjkdfbv,snbjcvhskdvkhfvbajsdbczxcvhxhvkjhgfjkd.",
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                              style:
                                  TextStyle(fontSize: 12, color: Colors.white),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Positioned(
                  bottom: 0,
                  child: ClipRRect(
                    borderRadius: const BorderRadius.only(
                        bottomLeft: Radius.circular(20)),
                    child: Image.asset(
                      "assets/buble.png",
                      height: 48,
                      width: 40,
                      color: const Color(0xFF801336),
                    ),
                  ),
                ),
                Positioned(
                    top: -20,
                    left: 0,
                    child: Stack(
                      alignment: Alignment.center,
                      children: [
                        Transform.rotate(
                          angle: pi,
                          child: Image.asset(
                            "assets/water.png",
                            height: 40,
                          ),
                        ),
                        Positioned(
                          top: 10,
                          child: Image.asset(
                            "assets/close.png",
                            height: 16,
                          ),
                        )
                      ],
                    ))
              ],
            ),
            behavior: SnackBarBehavior.floating,
            elevation: 0,
            backgroundColor: Colors.transparent,
          ));
        },
        child: const Text("Show Message"),
      )),
    );
  }
}
