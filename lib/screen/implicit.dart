import 'package:flutter/material.dart';

class ImplicitAnimation extends StatefulWidget {
  const ImplicitAnimation({super.key});

  @override
  State<ImplicitAnimation> createState() => _ImplicitAnimationState();
}

const defaultWidth = 100.0;

class _ImplicitAnimationState extends State<ImplicitAnimation> {
  var _isZoomIn = false;
  var _buttonTitle = "Zoom In";
  var _width = defaultWidth;
  var _curve = Curves.bounceOut;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                AnimatedContainer(
                  width: _width,
                  curve: _curve,
                  duration: const Duration(milliseconds: 370),
                  child: Image.asset("assets/wallpaper.jpg"),
                ),
              ],
            ),
            TextButton(
                onPressed: () {
                  setState(() {
                    _isZoomIn = !_isZoomIn;
                    _buttonTitle = _isZoomIn ? "Zoom Out" : "Zoom In";
                    _width = _isZoomIn
                        ? MediaQuery.of(context).size.width
                        : defaultWidth;
                    _curve = _isZoomIn ? Curves.easeIn: Curves.easeOutSine;
                  });
                },
                child: Text(_buttonTitle))
          ],
        ),
      ),
    );
  }
}
