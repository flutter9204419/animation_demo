import 'package:flutter/material.dart';
import 'dart:math' as math;

import 'dart:developer' as devtools show log;

extension Log on Object {
  void log() => devtools.log(toString());
}

class CircleClipper extends CustomClipper<Path> {
  const CircleClipper();
  @override
  Path getClip(Size size) {
    var path = Path();

    final rect = Rect.fromCircle(
        center: Offset(size.width / 2, size.height / 2),
        radius: size.width / 2);
    path.addOval(rect);

    return path;
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) => false;
}

Color getRandomColor() => Color(0xff000000 + math.Random().nextInt(0x00ffffff));

class TweenColorAnimation extends StatefulWidget {
  const TweenColorAnimation({super.key});

  @override
  State<TweenColorAnimation> createState() => _TweenColorAnimationState();
}

class _TweenColorAnimationState extends State<TweenColorAnimation> {
  var _color = getRandomColor();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: ClipPath(
          clipper: const CircleClipper(),
          child: TweenAnimationBuilder(
            duration: const Duration(seconds: 1),
            tween: ColorTween(begin: getRandomColor(), end: _color),
            onEnd: () {
              setState(() {
                _color = getRandomColor(); 
              });
            },
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.width,
              color: Colors.red,
            ),
            builder: (context, Color? color, child) {
              'Color Change'.log();
              // return child!;
              return ColorFiltered(
                  colorFilter: ColorFilter.mode(color!, BlendMode.srcATop),child: child,);
            },
          ),
        ),
      ),
    );
  }
}
