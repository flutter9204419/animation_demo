import 'package:animation/screen/3d.dart';
import 'package:animation/screen/curve_chained.dart';
import 'package:animation/screen/custom_flashbar.dart';
import 'package:animation/screen/drawer.dart';
import 'package:animation/screen/example1.dart';
import 'package:animation/screen/hero_animation.dart';
import 'package:animation/screen/implicit.dart';
import 'package:animation/screen/polygone_paint.dart';
import 'package:animation/screen/prompt_box.dart';
import 'package:animation/screen/tween_color.dart';
import 'package:flutter/material.dart';

class DisplayAnimation extends StatelessWidget {
  const DisplayAnimation({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
          AnimationButton(
            label: "Tween Animation",
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const MyHomePage(),
                  ));
            },
          ),
          AnimationButton(
            label: "Custom Snakbar",
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const FlashMessageScreen(),
                  ));
            },
          ),
          AnimationButton(
            label: "Chained Animation",
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const ChainedAnimationPage(),
                  ));
            },
          ),
          AnimationButton(
            label: "3D Animation",
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const ThreeDAnimation(),
                  ));
            },
          ),
          AnimationButton(
            label: "Hero Animation",
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const HeroAnimation(),
                  ));
            },
          ),
          AnimationButton(
            label: "Implicit Animation",
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const ImplicitAnimation(),
                  ));
            },
          ),
          AnimationButton(
            label: "TweenColor Animation",
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const TweenColorAnimation(),
                  ));
            },
          ),
          AnimationButton(
            label: "Polygone Custompaint Animation",
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const PolygoneAnimation(),
                  ));
            },
          ),
          AnimationButton(
            label: "3D Drawer Animation",
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const DrawerAnimation(),
                  ));
            },
          ),
          AnimationButton(
            label: "Prompt Box Animation",
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const PromptBoxAnimation(),
                  ));
            },
          ),
        ]),
      ),
    );
  }
}

class AnimationButton extends StatelessWidget {
  final Function() onPressed;
  final String label;
  const AnimationButton({
    super.key,
    required this.onPressed,
    required this.label,
  });

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(onPressed: onPressed, child: Text(label));
  }
}
